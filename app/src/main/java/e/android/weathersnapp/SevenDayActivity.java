package e.android.weathersnapp;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import e.android.weathersnapp.network.OneDayInfo;
import e.android.weathersnapp.network.WeatherApiGetter;

public class SevenDayActivity extends AppCompatActivity {
    public String LOGTAG = "FromMeForDebugging";
    private RecyclerView mDayRecyclerView;
    private List<OneDayInfo> mDays = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seven_day);

        new GetWeatherTask().execute();

        final DayAdapter adapter = new DayAdapter(this, mDays);

        mDayRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mDayRecyclerView.setAdapter(adapter);
    }

    private class DayAdapter extends RecyclerView.Adapter<DayHolder> {
        private final LayoutInflater mInflator;

        DayAdapter(Context context, List<OneDayInfo> days) {

            mInflator = LayoutInflater.from(context);
            mDays = days;
        }

        @NonNull
        @Override
        public DayHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

            View itemView = mInflator.inflate(R.layout.recyclerview_item, viewGroup, false);
            return new DayHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull DayHolder dayHolder, int position) {
            if (mDays != null) {
                final OneDayInfo current = mDays.get(position);
                dayHolder.txtHightTemp.setText(current.getTemp().getMax().toString());
                dayHolder.txtDate.setText(current.getDt().toString());
                dayHolder.txtCurrentTemp.setText(current.getTemp().getDay().toString());
                dayHolder.txtLowTemp.setText(current.getTemp().getMin().toString());


            }
        }

        @Override
        public int getItemCount() {
            if (mDays != null)
                return mDays.size();
            else return 0;
        }
    }

    //method to get a day at a certain position
    public OneDayInfo getDayInfoAtPosition(int position) {

        return mDays.get(position);
    }


    private class DayHolder extends RecyclerView.ViewHolder {

        private TextView txtDate, txtCurrentTemp, txtHightTemp, txtLowTemp;


        public DayHolder(View itemView) {

            super(itemView);

            txtDate = itemView.findViewById(R.id.txt_date);
            txtCurrentTemp = itemView.findViewById(R.id.txt_current_temp);
            txtHightTemp = itemView.findViewById(R.id.txt_temp_high);
            txtLowTemp = itemView.findViewById(R.id.txt_temp_low);
        }
    }


    public static class GetWeatherTask extends AsyncTask<Void, Void, List<OneDayInfo>> {

        @Override
        protected List<OneDayInfo> doInBackground(Void... params) {
            return new WeatherApiGetter().getWeather();

        }
    }
}
