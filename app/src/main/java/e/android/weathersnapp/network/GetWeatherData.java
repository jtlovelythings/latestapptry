package e.android.weathersnapp.network;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GetWeatherData {

    @GET("daily?")
    Call<ApiResponse> getWeatherInfo(@Query("zip") String zip_code,
                                     @Query("mode") String mode,
                                     @Query("units") String units,
                                     @Query("cnt") String cnt,
                                     @Query("appid") String appid);

    @GET("daily?")
    Call<City> getWeatherList(@Query("zip") String zip_code,
                              @Query("mode") String mode,
                              @Query("units") String units,
                              @Query("cnt") String cnt,
                              @Query("appid") String appid);

}
