package e.android.weathersnapp.network;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class AllOfIt {

    @SerializedName("city")
    @Expose
    private City city;
    @SerializedName("cod")
    @Expose
    private String cod;
    @SerializedName("message")
    @Expose
    private Float message;
    @SerializedName("cnt")
    @Expose
    private Integer cnt;
    @SerializedName("list")
    @Expose
    private ArrayList<List> list = null;

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public AllOfIt withCity(City city) {
        this.city = city;
        return this;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public AllOfIt withCod(String cod) {
        this.cod = cod;
        return this;
    }

    public Float getMessage() {
        return message;
    }

    public void setMessage(Float message) {
        this.message = message;
    }

    public AllOfIt withMessage(Float message) {
        this.message = message;
        return this;
    }

    public Integer getCnt() {
        return cnt;
    }

    public void setCnt(Integer cnt) {
        this.cnt = cnt;
    }

    public AllOfIt withCnt(Integer cnt) {
        this.cnt = cnt;
        return this;
    }


    public class City {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("coord")
        @Expose
        private Coord coord;
        @SerializedName("country")
        @Expose
        private String country;
        @SerializedName("population")
        @Expose
        private Integer population;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public City withId(Integer id) {
            this.id = id;
            return this;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public City withName(String name) {
            this.name = name;
            return this;
        }

        public Coord getCoord() {
            return coord;
        }

        public void setCoord(Coord coord) {
            this.coord = coord;
        }

        public City withCoord(Coord coord) {
            this.coord = coord;
            return this;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public City withCountry(String country) {
            this.country = country;
            return this;
        }

        public Integer getPopulation() {
            return population;
        }

        public void setPopulation(Integer population) {
            this.population = population;
        }

        public City withPopulation(Integer population) {
            this.population = population;
            return this;
        }
    }

    public class Coord {

        @SerializedName("lon")
        @Expose
        private Float lon;
        @SerializedName("lat")
        @Expose
        private Float lat;

        public Float getLon() {
            return lon;
        }

        public void setLon(Float lon) {
            this.lon = lon;
        }

        public Coord withLon(Float lon) {
            this.lon = lon;
            return this;
        }

        public Float getLat() {
            return lat;
        }

        public void setLat(Float lat) {
            this.lat = lat;
        }

        public Coord withLat(Float lat) {
            this.lat = lat;
            return this;
        }

    }

    public class List {

        @SerializedName("dt")
        @Expose
        private Integer dt;
        @SerializedName("temp")
        @Expose
        private Temp temp;
        @SerializedName("pressure")
        @Expose
        private Float pressure;
        @SerializedName("humidity")
        @Expose
        private Integer humidity;
        @SerializedName("weather")
        @Expose
        private java.util.List<Weather> weather = null;
        @SerializedName("speed")
        @Expose
        private Float speed;
        @SerializedName("deg")
        @Expose
        private Integer deg;
        @SerializedName("clouds")
        @Expose
        private Integer clouds;
        @SerializedName("rain")
        @Expose
        private Float rain;

        public Integer getDt() {
            return dt;
        }

        public void setDt(Integer dt) {
            this.dt = dt;
        }

        public List withDt(Integer dt) {
            this.dt = dt;
            return this;
        }

        public Temp getTemp() {
            return temp;
        }

        public void setTemp(Temp temp) {
            this.temp = temp;
        }

        public List withTemp(Temp temp) {
            this.temp = temp;
            return this;
        }

        public Float getPressure() {
            return pressure;
        }

        public void setPressure(Float pressure) {
            this.pressure = pressure;
        }

        public List withPressure(Float pressure) {
            this.pressure = pressure;
            return this;
        }

        public Integer getHumidity() {
            return humidity;
        }

        public void setHumidity(Integer humidity) {
            this.humidity = humidity;
        }

        public List withHumidity(Integer humidity) {
            this.humidity = humidity;
            return this;
        }

        public java.util.List<Weather> getWeather() {
            return weather;
        }

        public void setWeather(java.util.List<Weather> weather) {
            this.weather = weather;
        }

        public List withWeather(java.util.List<Weather> weather) {
            this.weather = weather;
            return this;
        }

        public Float getSpeed() {
            return speed;
        }

        public void setSpeed(Float speed) {
            this.speed = speed;
        }

        public List withSpeed(Float speed) {
            this.speed = speed;
            return this;
        }

        public Integer getDeg() {
            return deg;
        }

        public void setDeg(Integer deg) {
            this.deg = deg;
        }

        public List withDeg(Integer deg) {
            this.deg = deg;
            return this;
        }

        public Integer getClouds() {
            return clouds;
        }

        public void setClouds(Integer clouds) {
            this.clouds = clouds;
        }

        public List withClouds(Integer clouds) {
            this.clouds = clouds;
            return this;
        }

        public Float getRain() {
            return rain;
        }

        public void setRain(Float rain) {
            this.rain = rain;
        }

        public List withRain(Float rain) {
            this.rain = rain;
            return this;
        }

    }

    public class Temp {

        @SerializedName("day")
        @Expose
        private Float day;
        @SerializedName("min")
        @Expose
        private Float min;
        @SerializedName("max")
        @Expose
        private Float max;
        @SerializedName("night")
        @Expose
        private Float night;
        @SerializedName("eve")
        @Expose
        private Float eve;
        @SerializedName("morn")
        @Expose
        private Float morn;

        public Float getDay() {
            return day;
        }

        public void setDay(Float day) {
            this.day = day;
        }

        public Temp withDay(Float day) {
            this.day = day;
            return this;
        }

        public Float getMin() {
            return min;
        }

        public void setMin(Float min) {
            this.min = min;
        }

        public Temp withMin(Float min) {
            this.min = min;
            return this;
        }

        public Float getMax() {
            return max;
        }

        public void setMax(Float max) {
            this.max = max;
        }

        public Temp withMax(Float max) {
            this.max = max;
            return this;
        }

        public Float getNight() {
            return night;
        }

        public void setNight(Float night) {
            this.night = night;
        }

        public Temp withNight(Float night) {
            this.night = night;
            return this;
        }

        public Float getEve() {
            return eve;
        }

        public void setEve(Float eve) {
            this.eve = eve;
        }

        public Temp withEve(Float eve) {
            this.eve = eve;
            return this;
        }

        public Float getMorn() {
            return morn;
        }

        public void setMorn(Float morn) {
            this.morn = morn;
        }

        public Temp withMorn(Float morn) {
            this.morn = morn;
            return this;
        }
    }

    public class Weather {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("main")
        @Expose
        private String main;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("icon")
        @Expose
        private String icon;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Weather withId(Integer id) {
            this.id = id;
            return this;
        }

        public String getMain() {
            return main;
        }

        public void setMain(String main) {
            this.main = main;
        }

        public Weather withMain(String main) {
            this.main = main;
            return this;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public Weather withDescription(String description) {
            this.description = description;
            return this;
        }

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }

        public Weather withIcon(String icon) {
            this.icon = icon;
            return this;
        }

    }
}

