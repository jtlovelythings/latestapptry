package e.android.weathersnapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.util.ArrayList;

import e.android.weathersnapp.network.GetWeatherData;
import e.android.weathersnapp.network.RetrofitInstance;
import retrofit2.Call;


public class MainActivity extends AppCompatActivity {
    public ArrayList<String> oneDayList = new ArrayList<>();
    public String LOGTAG = "FromMeForDebugging";

    //private MySevenDayRecyclerViewAdapter sevenDayAdapter;
    private RecyclerView sevenDayRecyclerView;
    private Fragment sevenDayFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Converters convert = new Converters();

        GetWeatherData getWeatherData = RetrofitInstance.getRetrofitInstance().create(GetWeatherData.class);


        Call apiCall = getWeatherData.getWeatherInfo("98109,US", "json", "metric", "14", "f8fdae74c29544baebdb927d392c5538");
        Call getWeatherCityInfo = getWeatherData.getWeatherList("98109,US", "json", "metric", "14", "f8fdae74c29544baebdb927d392c5538");
        Log.i(LOGTAG, "URI" + apiCall.request().url() + "");
        Log.i(LOGTAG, getWeatherCityInfo.request().url() + "");

        Intent intentToStartWeatherReportActivity = new Intent(MainActivity.this, SevenDayActivity.class);
        startActivity(intentToStartWeatherReportActivity);


    }
}


