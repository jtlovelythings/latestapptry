package e.android.weathersnapp;

import java.util.Date;

public class Converters {


    public Date fromTimestamp(Long value) {
        return value == null ? null : new Date(value);
    }


    public Long dateToTimestamp(Date date) {
        if (date == null) {
            return null;
        } else {
            return date.getTime();
        }
    }


}
