package e.android.weathersnapp.network;

import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class WeatherApiGetter {

    public String LOGTAG = "FromMeForDebugging";

    public byte[] getUrlBytes(String urlSpec) throws IOException {

        URL url = new URL(urlSpec);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            InputStream in = connection.getInputStream();

            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new IOException(connection.getResponseMessage() + ": with " + urlSpec);
            }

            int bytesRead = 0;
            byte[] buffer = new byte[1024];

            while ((bytesRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, bytesRead);


            }
            out.close();
            return out.toByteArray();
        } finally {
            connection.disconnect();
        }

    }

    public String getUrlString(String urlSpec) throws IOException {


        return new String(getUrlBytes(urlSpec));

    }

    public List<OneDayInfo> getWeather() {

        List<OneDayInfo> days = new ArrayList<>();
        try {
            String weatherUrl = Uri.parse("http://api.openweathermap.org/data/2.5/forecast/")
                    .buildUpon()
                    .appendPath("daily")
                    .appendQueryParameter("zip", "98109")
                    .appendQueryParameter("mode", "json")
                    .appendQueryParameter("units", "metric")
                    .appendQueryParameter("cnt", "14")
                    .appendQueryParameter("appid", "f8fdae74c29544baebdb927d392c5538")
                    .build().toString();
            Log.i(LOGTAG, "Url: " + weatherUrl);
            String jsonString = getUrlString(weatherUrl);
            Log.i(LOGTAG, "Recieved JSON: " + jsonString);
            JSONObject jsonBody = new JSONObject(jsonString);
            parseItems(days, jsonBody);


            Log.i(LOGTAG, "JSON Object: " + jsonBody.toString());
        } catch (IOException e) {
            Log.e(LOGTAG, "Failed to get api data ", e);
        } catch (JSONException je) {
            Log.e(LOGTAG, "Failed to parse JSON", je);
        }
        return days;

    }

    private void parseItems(List<OneDayInfo> days, JSONObject jsonBody) throws JSONException {

        JSONArray daysObject = jsonBody.getJSONArray("list");
        Log.i(LOGTAG, daysObject.toString(4));


        for (int i = 0; i < daysObject.length(); i++) {

            JSONObject dayJsonObject = daysObject.getJSONObject(i);

            OneDayInfo day = new OneDayInfo();
            day.setDt(dayJsonObject.getInt("dt"));
            day.setClouds(dayJsonObject.getInt("clouds"));
            day.setSpeed(dayJsonObject.getDouble("speed"));
            days.add(day);
        }


    }
}


